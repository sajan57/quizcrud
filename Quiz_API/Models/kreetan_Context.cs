﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Quiz_API.Models
{
    public partial class kreetan_Context : DbContext
    {
        public kreetan_Context()
        {
        }

        public kreetan_Context(DbContextOptions<kreetan_Context> options)
            : base(options)
        {
        }

        public virtual DbSet<Quiz> Quizzes { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
//                optionsBuilder.UseSqlServer("Server=DESKTOP-57078PD;Database=kreetan_;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Quiz>(entity =>
            {
                entity.ToTable("Quiz");

                entity.Property(e => e.Answer).IsUnicode(false);

                entity.Property(e => e.Option1).IsUnicode(false);

                entity.Property(e => e.Option2).IsUnicode(false);

                entity.Property(e => e.Option3).IsUnicode(false);

                entity.Property(e => e.Option4).IsUnicode(false);

                entity.Property(e => e.Question).IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
